<?php

namespace App\Entity;

use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;

/**
 * Empleado
 *
 * @ORM\Table(name="empleado", indexes={@ORM\Index(name="area_id", columns={"area_id"})})
 * @ORM\Entity
 */
class Empleado{

  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var string
   *
   * @ORM\Column(name="nombre", type="string", length=255, nullable=false)
   */
  private $nombre;

  /**
   * @var string
   *
   * @ORM\Column(name="email", type="string", length=255, nullable=false)
   */
  private $email;

  /**
   * @var string
   *
   * @ORM\Column(name="sexo", type="string", length=1, nullable=false, options={"fixed"=true})
   */
  private $sexo;

  /**
   * @var int|null
   *
   * @ORM\Column(name="boletin", type="integer", nullable=true)
   */
  private $boletin;

  /**
   * @var string
   *
   * @ORM\Column(name="descripcion", type="text", length=65535, nullable=false)
   */
  private $descripcion;

  /**
   * @var \Areas
   *
   * @ORM\ManyToOne(targetEntity="Areas")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="area_id", referencedColumnName="id")
   * })
   */
  private $area;

  /**
   * @var int|null
   *
   * @ORM\Column(name="eliminar", type="integer", nullable=true)
   */
  private $eliminar;

  public function getId(): ?int{
    return $this->id;
  }

  public function getNombre(): ?string{
    return $this->nombre;
  }

  public function setNombre(string $nombre): self{
    $this->nombre = $nombre;
    return $this;
  }

  public function getEmail(): ?string{
    return $this->email;
  }

  public function setEmail(string $email): self{
    $this->email = $email;
    return $this;
  }

  public function getSexo(): ?string{
    return $this->sexo;
  }

  public function setSexo(string $sexo): self{
    $this->sexo = $sexo;
    return $this;
  }

  public function getBoletin(): ?int{
    return $this->boletin;
  }

  public function setBoletin(?int $boletin): self{
    $this->boletin = $boletin;
    return $this;
  }

  public function getDescripcion(): ?string{
    return $this->descripcion;
  }

  public function setDescripcion(string $descripcion): self{
    $this->descripcion = $descripcion;
    return $this;
  }

  public function getArea(): ?Areas{
    return $this->area;
  }

  public function setArea(?Areas $area): self{
    $this->area = $area;
    return $this;
  }

  public function getEliminar(): ?int{
    return $this->eliminar;
  }

  public function setEliminar(?int $eliminar): self{
    $this->eliminar = $eliminar;
    return $this;
  }
}
