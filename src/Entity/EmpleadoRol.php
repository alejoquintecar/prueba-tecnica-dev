<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EmpleadoRol
 *
 * @ORM\Table(name="empleado_rol", indexes={@ORM\Index(name="empleado_id", columns={"empleado_id"}), @ORM\Index(name="rol_id", columns={"rol_id"})})
 * @ORM\Entity
 */
class EmpleadoRol{

  /**
   * @var int
   *
   * @ORM\Column(name="id", type="integer", nullable=false)
   * @ORM\Id
   * @ORM\GeneratedValue(strategy="IDENTITY")
   */
  private $id;

  /**
   * @var \Empleado
   *
   * @ORM\ManyToOne(targetEntity="Empleado")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="empleado_id", referencedColumnName="id")
   * })
   */
  private $empleado;

  /**
   * @var \Roles
   *
   * @ORM\ManyToOne(targetEntity="Roles")
   * @ORM\JoinColumns({
   *   @ORM\JoinColumn(name="rol_id", referencedColumnName="id")
   * })
   */
  private $rol;

  public function getId(): ?int{
    return $this->id;
  }

  public function getEmpleado(): ?Empleado{
    return $this->empleado;
  }

  public function setEmpleado(?Empleado $empleado): self{
    $this->empleado = $empleado;
    return $this;
  }

  public function getRol(): ?Roles{
    return $this->rol;
  }

  public function setRol(?Roles $rol): self{
    $this->rol = $rol;
    return $this;
  }
}