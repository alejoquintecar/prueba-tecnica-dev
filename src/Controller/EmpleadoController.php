<?php

namespace App\Controller;

use Doctrine\Persistence\ManagerRegistry;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
// --- --- --- Entidades --- --- ---
use App\Entity\Empleado;
use App\Entity\EmpleadoRol;
// --- --- --- Form --- --- ---
use App\Form\EmpleadoType;
// // --- --- --- Services --- --- ---
// use App\Service\Globales\RegistroActividad;

class EmpleadoController extends AbstractController{

  // Ubicacion vista
  private $sVistaTxt  = "Lista";
  private $sModuloTxt = "Empleados";
  // Objetos
  private $oEntManager;

  public function __construct(ManagerRegistry $oDoctrine){
    $this->oEntManager = $oDoctrine->getManager();
  }


  public function index(): Response{

    $aDfColumnsButtons = $this->indexDfColumnsButtons();
    return $this->render('empleado/index.html.twig', [
      'vista_txt' => $this->sVistaTxt,
      'modulo_txt' => $this->sModuloTxt,
      'dfColumnsButtons' => json_encode($aDfColumnsButtons)
    ]);
  }

  /**
   * Datos usuarios Json
   * 
   * @return json [totalRows, data] Data Grilla.
   */
  public function indexJson(Request $oRequest, $bExportar = false): Response{

    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();

    if( $oRequest->isXmlHttpRequest() || $bExportar == true ){

      // --- --- --- --- Total Filas --- --- --- --- //
      $nTotalRegistros = 0;
      // if( $aDataGrilla["paginaActual"] == 1 && $exportar === false ){
        $oContador = $this->oEntManager->createQuery("SELECT COUNT(emp.id) AS totalRegistros
          FROM App\Entity\Empleado emp
        ");        
        $nTotalRegistros = $oContador->getSingleResult()['totalRegistros'];
      // }

      // DQL Data
      $oQueryEmpleados = $this->oEntManager->createQuery("SELECT emp.id, emp.nombre, emp.email,
        emp.sexo, emp.boletin, emp.descripcion, are.id as areaId, are.nombre as area,
        rol.id as rolId, rol.nombre as nameRol
        FROM App\Entity\Empleado emp
        JOIN App\Entity\Areas as are WITH are.id = emp.area
        JOIN App\Entity\EmpleadoRol as empRol WITH empRol.empleado = emp.id
        JOIN App\Entity\Roles as rol WITH rol.id = empRol.rol
        WHERE emp.eliminar = 0
        GROUP BY empRol.empleado
      ");
      $aQueryEmpleados = $oQueryEmpleados->getScalarResult();
      // --- --- --- Lógica --- --- --- //
      $aEmpleados = array();
      foreach( $aQueryEmpleados as $aItem ){
        // Boletin.
        $sBoletin = '';
        if( $aItem['boletin'] == 1 ) $sBoletin = "Si";
        else if( $aItem['boletin'] == 0 ) $sBoletin = "No";
        // Data.
        $aEmpleados[] = array(
          'id'           => $aItem['id'],
          'nombre'       => $aItem['nombre'],
          'email'        => $aItem['email'],
          'sexo'         => $aItem['sexo'],
          'area'         => $aItem['area'],
          'nameRol'      => $aItem['nameRol'],
          'boletin'      => $sBoletin,
          'modificar_id' => $aItem['id'],
          'eliminar_id'  => $aItem['id'],
        );
      }
      // Cierre de conexion y Respuesta.
      $this->oEntManager->getConnection()->close();
      $response->setContent(json_encode([
        'data' => $aEmpleados,
        'totalRows' => $nTotalRegistros
      ]));
    }else{
      $aJson['status'] = 0;
      $aJson['message'] = "Acción no valida";
      $response->setContent(json_encode($aJson));
    }
    return $response;
  }

  /**
   * Datos usuarios Json
   * 
   * @return json [status, message] Data Grilla.
   */
  public function indexNew(Request $oRequest): Response{
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();

    if( $oRequest->isXmlHttpRequest() ){

      // $em = $this->oEntityManager;
      $aDataForm = $oRequest->get('empleado');

      if( !is_null($aDataForm) ){

        $oEmpleado = $this->oEntManager->getRepository('App\Entity\Empleado')->findOneBy([
          'email' => $aDataForm['email']
        ]);

        if( is_null($oEmpleado) ){

          $nAreaId = $aDataForm['area'];
          $nboletin = (isset($aDataForm['boletin'])) ? 1 : 0;

          $oEmpleado = new Empleado();
          $oEmpleado->setNombre($aDataForm['nombre']);
          $oEmpleado->setEmail($aDataForm['email']);
          $oEmpleado->setSexo($aDataForm['sexo']);
          $oEmpleado->setBoletin($nboletin);
          $oEmpleado->setDescripcion($aDataForm['descripcion']);
          $oEmpleado->setArea($this->oEntManager->getReference('App\Entity\Areas', $nAreaId));
          // Guardado
          $this->oEntManager->persist($oEmpleado);
          $this->oEntManager->flush();
          foreach ($aDataForm['roles'] as $key => $nRolId) {
            $oEmpleadoRol = new EmpleadoRol();
            $oEmpleadoRol->setEmpleado($this->oEntManager->getReference('App\Entity\Empleado', $oEmpleado->getId()));
            $oEmpleadoRol->setRol($this->oEntManager->getReference('App\Entity\Roles', $nRolId));
            // Guardado.
            $this->oEntManager->persist($oEmpleadoRol);
            $this->oEntManager->flush();
          }

          $aJson['status'] = 1;
          $aJson['message'] = '';

        }else{
          $aJson['status'] = 0;
          $aJson['message'] = 'El empleado ingresado ya existe.';
        }
        $response->setContent(json_encode($aJson));
      }else{

        // Entity manager y Formulario
        $entity = new Empleado();
        $form = $this->createForm(EmpleadoType::class, $entity);

        // DQL Data
        $oQueryRoles = $this->oEntManager->createQuery("SELECT rol.id, rol.nombre
          FROM App\Entity\Roles rol
        ");
        $aQueryRoles = $oQueryRoles->getScalarResult();

        $aJson['entity'] = $entity;
        $aJson['action'] = 'new';
        $aJson['roles']   = $aQueryRoles;
        $aJson['form']   = $form->createView();
        return $this->render('empleado/mdlCrud.html.twig', $aJson);
      }
    }else{
      $aJson['status'] = 0;
      $aJson['msg'] = "Acción no valida";
      $response->setContent(json_encode($aJson));
    }
    return $response;
  }

  /**
   * Datos usuarios Json
   * 
   * @return json [status, message] Data Grilla.
   */
  public function indexEdit(Request $oRequest): Response{
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();

    if( $oRequest->isXmlHttpRequest() ){

      $registroId = $oRequest->get("registroId");
      $aDataForm = $oRequest->get('empleado');

      if( !is_null($aDataForm) ){

        $oRegistroActual = $this->oEntManager->getRepository('App\Entity\Empleado')->findOneById($registroId);
        $oEmpleadoEmail = $this->oEntManager->getRepository('App\Entity\Empleado')->findOneBy([
          'email' => $aDataForm['email']
        ]);
        if( $oEmpleadoEmail->getEmail() == NULL || $oRegistroActual->getEmail() == $oEmpleadoEmail->getEmail() ){

          $nAreaId = $aDataForm['area'];
          $nboletin = (isset($aDataForm['boletin'])) ? 1 : 0;

          $oRegistroActual->setNombre($aDataForm['nombre']);
          $oRegistroActual->setEmail($aDataForm['email']);
          $oRegistroActual->setSexo($aDataForm['sexo']);
          $oRegistroActual->setBoletin($nboletin);
          $oRegistroActual->setDescripcion($aDataForm['descripcion']);
          $oRegistroActual->setArea($this->oEntManager->getReference('App\Entity\Areas', $nAreaId));

          // Guardado
          $this->oEntManager->persist($oRegistroActual);
          $this->oEntManager->flush();
          // Eliminar Roles.
          $qrDeleteEmpleadoRol = $this->oEntManager->createQuery("DELETE 
            FROM App\Entity\EmpleadoRol empRol
            WHERE empRol.empleado = $registroId
          "); $qrDeleteEmpleadoRol->execute();
          // Asignar roles.
          foreach ($aDataForm['roles'] as $key => $nRolId) {
            $oEmpleadoRol = new EmpleadoRol();
            $oEmpleadoRol->setEmpleado($this->oEntManager->getReference('App\Entity\Empleado', $oRegistroActual->getId()));
            $oEmpleadoRol->setRol($this->oEntManager->getReference('App\Entity\Roles', $nRolId));
            // Guardado.
            $this->oEntManager->persist($oEmpleadoRol);
            $this->oEntManager->flush();
          }
          $aJson['status'] = 1;
          $aJson['message'] = 'Registro actualizado correctamente.';
        }else{
          $aJson['status'] = 0;
          $aJson['message'] = "El nuevo correo electrónico ya se encuentra registrado";
        }        
      }else{
        // Entity manager y Formulario.
        $oEntity = $this->oEntManager->getRepository(Empleado::class)->findOneById($oRequest->get("registroId"));
        $oEditForm = $this->createForm(EmpleadoType::class, $oEntity);

        // DQL Roles.
        $oQueryRoles = $this->oEntManager->createQuery("SELECT rol.id, rol.nombre
          FROM App\Entity\Roles rol
        "); $aQueryRoles = $oQueryRoles->getScalarResult();
        // DQL Roles data activa.
        $oQueryRolesActivos = $this->oEntManager->createQuery("SELECT rol.id, rol.nombre
          FROM App\Entity\EmpleadoRol rolAct
          JOIN App\Entity\Roles rol WITH rol.id = rolAct.rol
          WHERE rolAct.empleado = $registroId
        "); $aQueryRolesActivos = $oQueryRolesActivos->getScalarResult();

        // Data Form.
        $aJson['form']         = $oEditForm->createView();
        $aJson['roles']        = $aQueryRoles;
        $aJson['action']       = 'edit';
        $aJson['entity']       = $oEntity;
        $aJson['registroId']   = $registroId;
        $aJson['rolesActivos'] = $aQueryRolesActivos;

        return $this->render('empleado/mdlCrud.html.twig', $aJson);
      }
    }else{
      $aJson['status'] = 0;
      $aJson['message'] = "Acción no valida";
    }
    $response->setContent(json_encode($aJson));
    return $response;
  }

  /**
   * Datos usuarios Json
   * 
   * @return json [status, message] Data Grilla.
   */
  public function indexDelete(Request $oRequest): Response{
    $response = new Response();
    $response->headers->set('Content-Type', 'application/json');
    $aJson = array();

    if( $oRequest->isXmlHttpRequest() ){
      $nRegistroId = $oRequest->get('registroId');
      $oEmpleado = $this->oEntManager->getRepository('App\Entity\Empleado')->findOneBy([
        'id' => $nRegistroId
      ]);
      $oEmpleado->setEliminar(1);
      // Guardado
      $this->oEntManager->persist($oEmpleado);
      $this->oEntManager->flush();
      $this->oEntManager->getConnection()->close();

      $aJson['status'] = 1;
      $aJson['message'] = "Registro eliminado";
    }else{
      $aJson['status'] = 0;
      $aJson['message'] = "Acción no valida";
    }

    $response->setContent(json_encode($aJson));
    return $response;
  }

  // --- --- --- Definicion de columnas --- --- ---
  private function indexDfColumnsButtons(){

    $aDfColumns = [ 'columns' => [
      [ 'width' => 0, 'visible' => false, 'data' => 'id',        'title' => 'ID', ],
      [ 'width' => 100, 'data' => 'nombre',    'title' => '<i class="fa-solid fa-user text-info pe-2"></i>Nombre', ],
      [ 'width' => 100, 'data' => 'email',     'title' => '<i class="fa-solid fa-at text-info pe-2"></i>Email', ],
      [ 'width' => 50,  'data' => 'sexo',      'title' => '<i class="fa-solid fa-venus-mars text-info pe-2"></i>Sexo', ],
      [ 'width' => 100, 'data' => 'area',      'title' => '<i class="fa-solid fa-briefcase text-info pe-2"></i>Área', ],
      [ 'width' => 50,  'data' => 'boletin',   'title' => '<i class="fa-solid fa-envelope text-info pe-2"></i>Boletín', ],
      [ 'width' => 50, 'visible' => true, 'data' => 'modificar_id',  'title' => 'Modificar', 'cellRender' => [
        'render'  => 'buttons',
        'buttons' => [
          'editar' => [
            'id' => 'editar-registro',
            'text' => 'Editar',
            'title' => 'Editar Registro',
            'icons' => ['fa-solid fa-pen-to-square'],
            'class' => array('btn-outline-warning')
          ],
        ]
      ]],
      [ 'width' => 50, 'visible' => true, 'data' => 'eliminar_id',  'title' => 'Eliminar', 'cellRender' => [
        'render'  => 'buttons',
        'buttons' => [
          'eliminar' => [
            'id' => 'editar-registro',
            'text' => 'Editar',
            'title' => 'Editar Registro',
            'icons' => array('fa-regular fa-trash-can'),
            'class' => array('btn-danger')
          ],
        ]
      ]],
    ]];

    $aDfButtons = ['crear' => [
      'id' => 'crear-registro',
      'text' => 'Crear',
      'title' => 'Crear Registro',
      'icons' => array('fa-solid fa-user-plus'),
      'class' => array('btn-outline-success')
    ]];

    return [ 'columns' => $aDfColumns, 'buttons' => $aDfButtons ];
  }

}