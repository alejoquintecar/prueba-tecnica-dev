<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
// use Symfony\Component\HttpFoundation\Response;

class DefaultController extends AbstractController{

  public function index(Request $oRequest): RedirectResponse{
    // redirects to a route and maintains the original query string parameters
    return $this->redirectToRoute(
      'empleado_controller', $oRequest->query->all()
    );
  }

}