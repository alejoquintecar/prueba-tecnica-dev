<?php

namespace App\Form;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
// Entity.
use App\Entity\Areas;
use App\Entity\Empleado;
// Form Type.
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
// use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class EmpleadoType extends AbstractType{

  /**
   * @param FormBuilderInterface $builder
   * @param array $options
  */
  public function buildForm(FormBuilderInterface $builder, array $options){
      
    $builder
      ->add('nombre', TextType::class, array(
        'label' => 'Nombre completo',
        'label_attr' => ['class' => 'fw-bold'],
        'attr' => array(
          'class' => 'form-control',
          'required' => true
        )
      ))
      ->add('email', EmailType::class, array(
        'label' => 'Correo electrónico',
        'label_attr' => ['class' => 'fw-bold'],
        'attr' => array(
          'class' => 'form-control',
          'required' => true
        )
      ))
      ->add('area', EntityType::class, array(
        'label' => 'Área',
        'label_attr' => ['class' => 'fw-bold'],
        'class' => Areas::class,
        'query_builder' => function (EntityRepository $er) {
          return $er->createQueryBuilder('are')->orderBy('are.nombre', 'ASC');
        },
        'choice_label' => 'nombre',
        'attr' => array(
          'class' => 'form-control js-example-basic-single',
          'required' => true
        )
      ))
      ->add('descripcion', TextareaType::class, array(
        'label' => 'Descripción',
        'label_attr' => ['class' => 'fw-bold'],
        'attr' => array(
          'class' => 'form-control form-control-sm',
          'required' => true
        )
      ));
  }

   /**
    * @param OptionsResolver $resolver
   */
   public function configureOptions(OptionsResolver $resolver){
      $resolver->setDefaults([
         'data_class' => Empleado::class,
         'attr' => array('id' => 'formEmpleado'),
      ]);
   }
}