/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */
// --- --- --- Css --- --- ---
import './styles/app.scss';
// --- --- END Css --- --- ---

// --- --- --- Js --- --- ---
// Import
import $ from 'jquery';
import swal from 'sweetalert2';
import { Tooltip, Modal, Bootstrap } from "bootstrap/dist/js/bootstrap.min.js";

// --- --- --- Variables --- --- ---
global.$ = global.jQuery = $;
global.Modal = Modal;
global.Tooltip = Tooltip;
global.Bootstrap = Tooltip;
// Modal Global
global.modalGlobal = new Modal(document.getElementById('modalGlobal'), {
  backdrop: true, keyboard: false
});
// --- --- END Variables --- --- ---




// Acciones por defecto
$(document).ready(function(){

  $('#btn-save-modalGlobal').on('click', function(){

    console.log(1212);

    let nInputEmpty = 0;
    $('#form-crud').find('input').each(function( index, element ){
      console.log(  );
      if( $(element).attr('required') == 'required' && 
        $(element).attr('type') != 'checkbox' && $(element).attr('type') != 'password' && $(element).val().trim() == ''
      ) nInputEmpty++;
    });

    $('#contenidoModalGlobal').find('#btn-mdl-submit').trigger('click');
    if( nInputEmpty > 0 ){
      Swal.fire({
        icon: 'info',
        title: 'Diligencie todos los campos.',
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 4500,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer),
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      });
    }
  });

  // --- Loading Hide
  $('#loading').hide();
  $('[data-bs-toggle="tooltip"]').tooltip() // to enable tooltips, with default configuration
});