// --- --- --- Js --- --- ---
// import IMask from 'imask';
import swal from 'sweetalert2';
import '../globales/datatables/jquery-datatables';
// import { Tooltip, Modal } from "bootstrap/dist/js/bootstrap.min.js";
import MdlCrud from "./mdlCrud";
// --- --- END Js --- --- ---

// --- --- --- Variables globales --- --- ---
oSwal = swal;
let oMdlCrud = new MdlCrud();
// --- --- END Variables globales --- --- ---

$(document).ready(function(){

  oMyDatatable = $.fn.ALEJO_Datatable({
    idDiv: "div-datatable",
    idTable: "my-datatable",
    ajaxGetData: {
      urlAjax: aRoutesUrls.indexJson, ORDER_SORD: "DESC", ORDER_SIDX: "id"
    },
    datatableOption:{
      defsColumn: aDfColumnsButtons.columns
    },
    buttons:{
      'plugins': ['refresh', 'search'],
      'add': aDfColumnsButtons.buttons
    }
  });

  // Crear Registro
  $('#my-datatable').on('click', '#my-datatable-crear-registro', function(){
    let oBtn = $(this);
    let sBtnIcons = '';
    $.each(oBtn.find('i'), function (index, icon) {
      $(icon).addClass('text-info');
      sBtnIcons += $(icon).prop('outerHTML');
      $(icon).removeClass('text-info');
    });
    // Loading Hide
    $("#loader").show();
    $('#tituloModalGlobal').html( 'Crear Empleado&nbsp;'+ sBtnIcons );
    $('#contenidoModalGlobal').empty().load(aRoutesUrls.indexNew, function (){
      $('#modalGlobal').modal({backdrop: true,keyboard: false});
      $('#modalGlobal').modal('show');
      oMdlCrud.init();
      $("#loader").hide();
    });
  });

  // Crear Registro
  $('#my-datatable').on('click', '.btn-datatables-editar', function(){
    let aRowsSelected = oMyDatatable.getRowsSelect();
    if( aRowsSelected[0] ){
      let oBtn = $(this);
      let sBtnIcons = '';
      aRowsSelected = aRowsSelected[0];
      $.each(oBtn.find('i'), function (index, icon) {
        $(icon).addClass('text-info');
        sBtnIcons += $(icon).prop('outerHTML');
        $(icon).removeClass('text-info');
      });
      // Titulo Modal
      $('#tituloModalGlobal').html('Editar Empleado:&nbsp;' + sBtnIcons );
      $('#contenidoModalGlobal').empty().load(aRoutesUrls.indexEdit, { registroId: aRowsSelected.id }, function (){
        $('#modalGlobal').modal({backdrop: true,keyboard: false});
        $('#modalGlobal').modal('show');
        oMdlCrud.init();
        $('#loader').hide();
      });

    }else{
      Swal.fire({
        icon: 'info',
        toast: true,
        timer: 4500,
        position: 'top-end',
        text: 'Por favor seleccione una fila de la tabla.',
        showConfirmButton: false,
        showCloseButton: true,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer),
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      });
    }
  });

  // Eliminar registro
  $('#my-datatable').on('click', '.btn-datatables-eliminar', function(){

    let oBtn = $(this);
    let nRegistroId = oBtn.data('registro-id');
    let aRowsSelected = oMyDatatable.getRowsSelect();
    if( aRowsSelected[0] ){

      Swal.fire({
        icon: 'warning',
        title: "Desea eliminar este empleado?",
        html: `<b class='h4'>${aRowsSelected[0].nombre}</b>`,
        confirmButtonText: 'Si',
        cancelButtonText: 'No',
        showCancelButton: true,
        cancelButtonColor: '#d33',
        confirmButtonColor: '#198754',
        position: 'center',
        showConfirmButton: true,
        showCloseButton: true,
        showCancelButton: true
      }).then((result) => {
        if( result.isConfirmed ){
          $.ajax({
            url: aRoutesUrls.indexDelete,
            data: { registroId: aRowsSelected[0].id },
            type: 'post',
            beforeSend: function(){
              $('#loading').show();
            },
            success: function(data){
              console.log(data);
              $("#loading").hide();
              Swal.fire({
                icon: ( data.status == 1 ) ? 'success' : 'info',
                title: data.message,
                toast: true,
                timer: 6000,
                position: 'top-end',
                showConfirmButton: false,
                showCloseButton: true,
                timerProgressBar: true,
                didOpen: (toast) => {
                  toast.addEventListener('mouseenter', Swal.stopTimer),
                  toast.addEventListener('mouseleave', Swal.resumeTimer)
                }
              });
              if( data.status == 1 || data.status == 2 ) location.reload();
            },
            error: function(data){
              $('#loading').hide();
            }
          });
        }
      });
    }else{
      Swal.fire({
        icon: 'info',
        toast: true,
        timer: 4500,
        position: 'top-end',
        text: 'Seleccione una fila de la tabla.',
        showConfirmButton: false,
        showCloseButton: true,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer),
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
      });

    }
  });

});