# Prueba Tecnica -> Nexura

## Installation

Se requiere tener instalado ```Composer```, ```Npm```, ```Php 8.1.13```
Posteriormente ejecutar los comandos:
```composer install```
```npm install```
```npm run dev```

### Crear - VirtualHost wampg4 (C:\wamp64\bin\apache\apache2.4.54.2\conf\httpd.conf)
```
# Symfony - 6.2
<VirtualHost *:80>
  DocumentRoot "${INSTALL_DIR}/www/prueba-tecnica-dev/public"
  ServerName prueba-tecnica-dev.com
  ServerAlias prueba-tecnica-dev.com
  <Directory "${INSTALL_DIR}/www/prueba-tecnica-dev/public">
    Options Indexes FollowSymLinks
    AllowOverride All
    Order Deny,Allow
    Allow from all
  </Directory>
</VirtualHost>
```


### Añadir el VirtualHost a los host locales (C:\Windows\System32\drivers\etc\hosts)

127.0.0.1 prueba-tecnica-dev.com

###Cualquier Duda quedo Atento
